-- Format for overwrittung defines values:
-- 
-- NDefines.NDiplomacy.MAX_CLIENT_STATES = 20

NDefines.NGame.END_DATE = "9999.12.12"

NDefines.NDiplomacy.OFFENSIVE_WAR_COOLDOWN = 0			-- Years between when you can call a country into an offensive war on your behalf
	
NDefines.NDiplomacy.AE_OTHER_CONTINENT = 0
NDefines.NDiplomacy.AE_SAME_CULTURE = 0
NDefines.NDiplomacy.AE_SAME_CULTURE_GROUP = 0
NDefines.NDiplomacy.AE_INFIDEL_CONQUEST = 0				-- different religion group conquered same religion province
NDefines.NDiplomacy.AE_SAME_RELIGION = 0
NDefines.NDiplomacy.AE_SAME_RELIGION_GROUP = 0
NDefines.NDiplomacy.AE_DIFFERENT_RELIGION = 0
NDefines.NDiplomacy.AE_HRE_INTERNAL = 0
NDefines.NDiplomacy.AE_ATTACKER_DEVELOPMENT = 0			-- +50% cap (at 1000 development)
NDefines.NDiplomacy.AE_DEFENDER_DEVELOPMENT = 0			-- -50% cap (at 1000 development)
NDefines.NDiplomacy.AE_DISTANCE_BASE = 0
NDefines.NDiplomacy.AE_SAME_OVERLORD = 0
	
NDefines.NDiplomacy.PO_DEMAND_PROVINCES_AE = 0	 		-- _DDEF_PO_DEMAND_PROVINCES_AE = 10, (Per development)
NDefines.NDiplomacy.PO_RETURN_CORES_AE = 0	 			-- (Per core, only applied if returning cores to vassals of winner)

NDefines.NCountry.PS_DEMAND_NON_WARGOAL_PROVINCE = 0
NDefines.NCountry.PS_DEMAND_NON_WARGOAL_PEACE = 0

NDefines.NEconomy.GOLD_MINE_DEPLETION_THRESHOLD = 10		-- Gold mines above production level or above can be depleted
NDefines.NEconomy.GOLD_MINE_DEPLETION_CHANCE = 1		-- Chance of gold mine being depleted (yearly, per production above threshold) 

NDefines.NAI.DEVELOPMENT_CAP_BASE = 100					-- AI will not develop provinces that have more development than this or DEVELOPMENT_CAP_MULT*original development (whichever is bigger)
NDefines.NAI.DEVELOPMENT_CAP_MULT = 100
